#!/usr/bin/make -f
# -*- mode: makefile -*-
# $Id: autogen.sh,v 1.2 2006-02-03 22:00:42 cholm Exp $
#
#    Example offline project
#    Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public License 
#   as published by the Free Software Foundation; either version 2 of 
#   the License, or (at your option) any later version.  
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details. 
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
#   MA 02111-1307  USA  
#
PREFIX		= $(HOME)/tmp
CONFFLAGS	= --prefix=$(PREFIX) 
AUTOMAKEVERSION = -1.9
PACKAGE		:= $(strip $(shell grep AC_INIT configure.ac |\
			sed 's/.*([^,]*,\([^,]*\),[^,]*,\[*\(.*\)\]*).*/\2/'))
VERSION		:= $(strip $(shell grep AC_INIT configure.ac |\
			sed 's/.*([^,]*,\([^,]*\),[^,]*,\(.*\)).*/\1/'))
ifneq ($(project),)
ifeq ($(project_name),)
project_name	:= $(shell echo $(project) | \
			sed -e 's/^\(.\)/\U\1/' -e 's/\(_\)/ /g')
endif
ifeq ($(Project),)
Project		:= $(shell echo $(project) | sed 's/^\(.\)/\U\1/')
endif
ifeq ($(PROJECT),)
PROJECT		:= $(shell echo $(project) | tr '[a-z]' '[A-Z]')
endif
ifeq ($(library),)
library		:= $(project)
endif
ifeq ($(date),)
date		:= $(shell date +"%B %e, %Y")
endif
endif

PROJ_FILES	:= $(shell find . -name "project*" -and -not -type d)
SKEL_FILES	:= $(shell find . | \
  xargs grep -i -l "@project@\|@project_name@\|@library@" | grep -v autogen.sh)

all:	setup
	./configure $(CONFFLAGS)

noopt:	setup
	./configure --disable-optimization $(CONFFLAGS)

setup:	ChangeLog configure 

config/ltmain.sh:
	libtoolize --automake --copy

config/config.hh.in:configure.ac 
	autoheader

aclocal.m4: configure.ac acinclude.m4 config/ltmain.sh
	aclocal$(AUTOMAKEVERSION) -I .

Makefile.in:configure.ac Makefile.am aclocal.m4 
	automake$(AUTOMAKEVERSION) --add-missing  --copy

configure:Makefile.in configure.ac 
	autoconf 

make:	all
	$(MAKE) -f Makefile 

dists:
	$(MAKE) dist
	$(MAKE) tar-ball -C doc


ChangeLog:
	rm -f $@
	touch $@
	rcs2log > $@

show:
	@echo "$(PACKAGE) version $(VERSION)"
clean: 
	-if test -f Makefile ; then $(MAKE) clean ; fi 
	find . -name Makefile.in 	| xargs rm -f 
	find . -name "*~" 		| xargs rm -f 
	find . -name core 		| xargs rm -f 
	find . -name .libs 		| xargs rm -rf 
	find . -name .deps 		| xargs rm -rf 
	find . -name "*.lo" 		| xargs rm -f 
	find . -name "*.o" 		| xargs rm -f 
	find . -name "*.la" 		| xargs rm -rf
	find . -name "*.log" 		| xargs rm -rf
	find . -name "semantic.cache" 	| xargs rm -rf
	find . -name "*Dict.*" 		| xargs rm -rf

	rm -f   Makefile 		\
		project/Makefile	\
		doc/Makefile
	rm -f 	config/missing       	\
	  	config/mkinstalldirs 	\
	  	config/ltmain.sh     	\
	  	config/config.guess 	\
		config/config.sub 	\
		config/install-sh 	\
		config/ltconfig 	\
		config/config.hh 	\
		config/config.hh.in 	\
		config/stamp-h		\
		config/stamp-h.in	\
		config/depcomp		\
		config/stamp-h1

	rm -rf 	aclocal.m4 		\
		autom4te.cache		\
		config.cache 		\
		config.status  		\
		config.log 		\
		configure 		\
		INSTALL 		\
		ChangeLog		\
		configure-stamp		\
		build-stamp		\
		libtool			\
		libtool.m4		\
		ltoptions.m4		\
		ltsugar.m4		\
		ltversion.m4		\
		log

	rm -rf  doc/html			\
		doc/xml				\
		doc/latex			\
		doc/man				\
		doc/doxyconfig			\
		doc/header.html			\
		doc/$(PACKAGE).tags

	rm -rf  $(PACKAGE)-*.tar.gz 	

make_project:
ifeq ($(project),)
	@echo "Project name not set" 
else
	@if test -d ../$(project) ; then \
		echo "Directory ../$(project) already exists"; exit 1; fi 
	(cd ../ && cp -a skeleton $(project))
	(cd ../$(project) &&					\
		./autogen.sh project=$(project) 		\
			     Project=$(Project)			\
			     PROJECT=$(PROJECT)			\
			     project_name=$(project_name)	\
			     library=$(library) fill)
	(cd ../$(project) && find . -name "CVS" | xargs rm -rf)
endif

fill:
ifneq ($(project),)
	@echo "project=$(project)"
	@echo "Project=$(Project)"
	@echo "PROJECT=$(PROJECT)"
	@echo "project_name=$(project_name)"
	@echo "library=$(library)"
	@echo "date=$(date)"
	@echo "SKEL_FILES=$(SKEL_FILES)"
	@echo "PROJ_FILES=$(PROJ_FILES)"
	@$(foreach f, $(SKEL_FILES), \
	    echo "Substuting in $(f)" ; \
	    sed -e 's/@project@/$(project)/g'		\
		-e 's/@Project@/$(Project)/g'		\
		-e 's/@PROJECT@/$(PROJECT)/g'		\
		-e 's/@project_name@/$(project_name)/g'	\
		-e 's/@library@/$(library)/g'		\
		-e 's/@date@/$(date)/g'			\
		< $(f) > $(f).tmp ; 			\
		mv $(f).tmp $(f);)
	@$(foreach f, $(PROJ_FILES), \
	    n=`echo $(f) | sed 's/project/$(project)/'` ; \
	    mv -v $(f) $$n;)
	mv project $(project)
endif

#
# EOF
#
