dnl -*- mode: autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.2 2006-02-04 04:17:30 cholm Exp $
dnl 
dnl ------------------------------------------------------------------
dnl
dnl Macro for optionally turning debugging on or off.  Use like
dnl
dnl    AC_DEBUG
dnl
dnl This will give you the command line arguments `--enable-debug' and
dnl `--disable-debug'.   If the user passes `--enable-debug' then the
dnl macro does nothing to CFLAGS and CXXFLAGS (`-g' is in there
dnl per-default), and defines the pre-processor macro `NDEBUG'.  If
dnl the user specifies `--disable-debug', the script takes out the
dnl `-g' from CFLAGS and CXXFLAGS, and defines the pre-processor macro 
dnl `DEBUG'. 
dnl
dnl If you project does not use C++, take out the 
dnl `AC_REQUIRE([AC_PROG_CXX])' and remove references to CXXFLAGS 
dnl
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AH_TEMPLATE(NDEBUG, [Defined if no debugging is requested])
  AH_TEMPLATE(DEBUG,  [Defined if debugging is requested])
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])],
    [],[enable_debug=yes])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  if test "x$enable_debug" = "xno" ; then 
     AC_DEFINE(NDEBUG)
  else
     AC_DEFINE(DEBUG)
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
dnl Macro for optionally turning optimisation on or off.  Use like
dnl
dnl    AC_OPTIMIZATION
dnl
dnl This will give you the command line arguments
dnl `--enable-optimization' and `--disable-optimization'.   If the
dnl user passes `--enable-optimization' then the 
dnl macro makes sure that `-O2' is in CFLAGS and CXXFLAGS.  If the
dnl user passes `--enable-optimization=<level>, the `-0<level>' put in 
dnl CFLAGS and CXXFLAGS.  If the user passes `--disable-optimization',
dnl the script takes puts `-O' in CFLAGS and CXXFLAGS.
dnl `OPTIMIZATION'. 
dnl
dnl If you project does not use C++, take out the 
dnl `AC_REQUIRE([AC_PROG_CXX])' and remove references to CXXFLAGS 
dnl
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])],
    [],[enable_optimization=yes])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl
dnl EOF
dnl
