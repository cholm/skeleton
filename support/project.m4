dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: project.m4,v 1.1.1.1 2006-02-03 21:31:05 cholm Exp $ 
dnl  
dnl  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_ROOT_@PROJECT@([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_@PROJECT@],
[
    AC_REQUIRE([AC_ROOT_FRAMEWORK])
    AC_ARG_WITH([@project@-prefix],
        [AC_HELP_STRING([--with-@project@-prefix],
		[Prefix where @project_name@ is installed])],
        @project@_prefix=$withval, @project@_prefix="")

    AC_ARG_WITH([@project@-url],
        [AC_HELP_STRING([--with-@project@-url],
		[Base URL where the @project_name@ dodumentation is installed])],
        @project@_url=$withval, @project@_url="")
    if test "x${@PROJECT@_CONFIG+set}" != xset ; then 
        if test "x$@project@_prefix" != "x" ; then 
	    @PROJECT@_CONFIG=$@project@_prefix/bin/@project@-config
	fi
    fi   
    AC_PATH_PROG(@PROJECT@_CONFIG, @project@-config, no)
    @project@_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for @project_name@ version >= $@project@_min_version)

    @project@_found=no    
    if test "x$@PROJECT@_CONFIG" != "xno" ; then 
       @PROJECT@_CPPFLAGS=`$@PROJECT@_CONFIG --cppflags`
       @PROJECT@_INCLUDEDIR=`$@PROJECT@_CONFIG --includedir`
       @PROJECT@_LIBS=`$@PROJECT@_CONFIG --libs`
       @PROJECT@_LIBDIR=`$@PROJECT@_CONFIG --libdir`
       @PROJECT@_LDFLAGS=`$@PROJECT@_CONFIG --ldflags`
       @PROJECT@_PREFIX=`$@PROJECT@_CONFIG --prefix`
       
       @project@_version=`$@PROJECT@_CONFIG -V` 
       @project@_vers=`echo $@project@_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       @project@_regu=`echo $@project@_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $@project@_vers -ge $@project@_regu ; then 
            @project@_found=yes
       fi
    fi
    AC_MSG_RESULT($@project@_found - is $@project@_version) 
    AC_MSG_CHECKING(where the Framework documentation is installed)
    if test "x$@project@_url" = "x" && \
	test ! "x$@PROJECT@_PREFIX" = "x" ; then 
       @PROJECT@_URL=${@PROJECT@_PREFIX}/share/doc/@project@/html
    else 
	@PROJECT@_URL=$@project@_url
    fi	
    AC_MSG_RESULT($@PROJECT@_URL)
   
    if test "x$@project@_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(@PROJECT@_URL)
    AC_SUBST(@PROJECT@_PREFIX)
    AC_SUBST(@PROJECT@_CPPFLAGS)
    AC_SUBST(@PROJECT@_INCLUDEDIR)
    AC_SUBST(@PROJECT@_LDFLAGS)
    AC_SUBST(@PROJECT@_LIBDIR)
    AC_SUBST(@PROJECT@_LIBS)
])

dnl
dnl EOF
dnl 
